$`x`$ in $y$
$x$ in $y$

# Nearest neighbor

A number of cities arranged of a plane that has been divided up like an ordinary
Cartesian plane. Each city is located at an integral \$`(x,y)`$ intersection. City
names and location are given in the form of three arrays: $`c`$, $`x`$ and $`y`$, aligned by
index to provide the city name $`(c[i])`$ and its coordinates, $`(x[i], y[i])`$. For
each city queried. Determine the name of the nearest other city that shares
either an $`x`$ or a $`y`$ coordinate with the queried city.
If no other cities share an $`x`$ or a $`y`$ coordinate, return $NONE$. If two cities have
the same distance to the queried city, $`q[i]`$, consider the one with
alphabetically smaller name (i.e. 'ab' < 'aba' < 'abb') as the clother one. The
distance here denotes the Euclidean distance on the plane: difference in $`x`$ plus
difference in $`y`\$.

![alt text](img/bolt_case_3_pic_1.png)

For example, there are \$`n = 3`$ cities, $`c = [c1, c2, c3]`$, at locations $`x = [1,2,3]`$
and $`y = [3,2,3]`$, so points $`(3,3)`$, $`(2,2)`$ and $`(1,3)`$. Our queries are $`q = [c1,c2,c3]`$. The nearest city to $`c1`$ is $`c3`$ which shares a y value $`(distance = (1-3) + (3-3) = |-2|)`$. City $`c2`$ does not have a nearest city as none shares an $`x`$ or
y with c2, so this query would return NONE. A query of $`c3`$ would return $`c1`$ based
on the first calculation. The return array after all queries is $`[c3, NONE, c1]`\$.

#### Function description

Complete the function closestStraightCity. The function must return an array of
m strings, where the i<sup>th</sup> element of this array denotes the
answer to the i<sup>th</sup> query.

closestStraightCity has the following parameter(s):
\$`c[c[0],...c[n-1]]`$: an array of strings
$`x[x[0],...x[n-1]]`$: an array of integers
$`y[y[0],...y[n-1]]`$: an array of integers
$`q[q[0],...q[m-1]]`\$: an array of strings

#### Constraints

- $`1 \le n,m \le 10^5`$
- $`1 \le x[i], y[i] \le 10^9`$
- $`1 \le | q[i] |, | c[i]| \le 10`$
- Each character of all \$`c[i]`$ and $`q[i]`$ $`\in`$ $`ascii[a-z]`\$
- All city name values $`c[i]`$, are unique
- All cities have unique coordinates

#### Sample case 0

_Sample input 0_

```
const n = 3
const m = 3
const c = ['fastcity', 'bigbanana', 'xyz'];
const x = [23, 23, 23];
const y = [1, 10, 20];
const q = ['fastcity', 'bigbanana', 'xyz'];

```

_Sample output 0_

```
['bigbanana', 'fastcity', 'bigbanana']
```

_Explanation 0_

![alt text](img/bolt_case_3_pic_2.png)

There are 3 cities in the input with the corresponding coordinates: _fastcity_ =
\$`(23,1)`$, _bigbanana_ = $`(23,10)`$, _xyz_ = $`(23,20)`$. Distances from fastcity are $`9`$
to _bigbanana_ and $`19`$ to _xyz_, and _bigbanana_ is $`10`\$ from _xyz_. There is are 3 queries
to answer. The first of them asks for the closest city to _fastcity_ wich has
exactly one coordinate different. Both _bigbanana_ and _xyz_ have exactly one
coordinate difference to _fastcity_, but _bigbanana_ is closer to _fastcity_, so it is
the answer. The second query asks for the closest straight city to _bigbanana_,
and _fastcity_ is one unit closer than _xyz_, so fastcity os the answer. The third
query asks for the closest straight city to _xyz_, and _bigbanana_ is closer than
_fastcity_, so _bigbanana_ is the answer.

#### Sample case 1

_Sample input 1_

```
const n = 5
const m = 5
const c = ['green', 'red', 'blue', 'yellow', 'pink'];
const x = [100, 200, 300, 400, 500];
const y = [100, 200, 300, 400, 500];
const q = ['green', 'red', 'blue', 'yellow', 'pink'];
```

_Sample output 1_

```
['NONE', 'NONE', 'NONE', 'NONE', 'NONE']
```

_Explanation 1_

![alt text](img/bolt_case_3_pic_3.png)

None of the cities share a row or a column, so none meet the criteria for being closest.
