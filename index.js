function closestStraightCity(c, x, y, q) {
  // Построить points для городов из q
  // Обходим q
  // Для каждого q находим город лежащий на той же x или y из с
  // Для каждого найденного города считаем Euclidian Distance
  // Записывает наименьшее Euclidian Distance и что это за город
  // Если нет города лежащего на той же X или Y записываем NONE

  let tmpRange = []; // [cityNameFrom, cityNameTo, distance]
  let points = []; // [searchCity, indexInC, x, y]
  let resArray = []; // ["city",..,q]

  // TODO: можем отказаться от построения, и проверять это все сразу в обхде
  // points, котороый теперь станет обходом q.

  // строим points для каждого q
  for (let index = 0; index < q.length; index++) {
    const searchCity = q[index];
    const indexInC = c.indexOf(searchCity);
    points.push([searchCity, indexInC, [x[indexInC], y[indexInC]]]);
  }

  // Обходим points
  for (let index = 0; index < points.length; index++) {
    const cityNameFrom = points[index][0];
    const cityIndexInC = points[index][1];
    const cityFromX = points[index][2][0];
    const cityFromY = points[index][2][1];
    // let rangeMin = undefined;

    // ищем совпадения по X или Y обходя с
    // в случае uniq(x[i]) по всей длинне и uniq(q[i]) + q.length == x.length мы
    // все равно сделаем x.length, так что не вижу смысла городить оптимизацию
    // тут, обходя x или y.
    for (let jindex = 0; jindex < c.length; jindex++) {
      let cityNameTo = c[jindex];
      let cityToX = x[jindex];
      let cityToY = y[jindex];
      let isEqualCity = cityIndexInC == jindex;
      let isEqualX = cityFromX == cityToX;
      let isEqualY = cityFromY == cityToY;

      if (!isEqualCity && (isEqualX || isEqualY)) {
        let diffX = Math.abs(cityFromX - cityToX);
        let diffY = Math.abs(cityFromY - cityToY);
        // TODO: Выяснить как именно идет сравнение по алфавиту через встроеннй
        // sort(). Почему 'aab' > 'aac' = false? Подозреваю что по char-number:
        // поэтому: b < c = true
        // TODO: тут можно завести счетчик ограничения range и не добавлять
        // заведомо ненужный в tmpRange чтобы потом sort'у был полегче. К
        // примеру:
        // - Изначально считаем что rangeMin = максимальный = 10^9
        //    - если новый range > rangeMin, тогда НЕ пишем в tmpRange
        //    - если новый range < rangeMin, тогда очищаем tmpRange и пишем туда
        //      новый range
        //    - если новый range = rangeMin, тогда:
        //        - сравниваем город в tmpRange с текущим по алфавиту
        //            - если > tmpRange = []; tmpRange.push(больший)
        //            - если = НЕ пишем в tmpRange
        // В итоге в tmpRange у нас всегда один город и sort()
        // дальше может быть вобще не нужен

        if (!tmpRange.length) {
          tmpRange.push([cityNameFrom, cityNameTo, diffX + diffY]);
        } else {
          let prevRange = tmpRange[0][2];
          let currRange = diffX + diffY;
          let prevCity = tmpRange[0][1];
          let currCity = cityNameTo;

          if (currRange < prevRange) {
            tmpRange = [];
            tmpRange.push([cityNameFrom, cityNameTo, currRange]);
          }
          if (currRange == prevRange && currCity < prevCity) {
            tmpRange = [];
            tmpRange.push([cityNameFrom, cityNameTo, currRange]);
          }
        }
      }
    }

    if (tmpRange.length == 0) {
      resArray.push('NONE');
    } else {
      resArray.push(tmpRange[0][1]);
    }

    tmpRange = [];
  }

  return resArray;
}

module.exports = closestStraightCity;
