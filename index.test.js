// TODO:

// Что значат ограничения? 1 <= n,m <= 10^5 - n может быть меньше m ? Тогда как
// быть? Нет координат для m[i] если только в этом случае мы н должны вернуть
// NONE или если m[i...n] могут быть не уникальными.

// Что значит ограничение? 1 <= | q[i] |, | c[i] | <= 10. Если q[i] строка, то
// это количество сивлолов? Тогда почему по модулю? И зачем ) Чтоыб не думать
// про память и ограничения на названия?

const f = require('./index');

describe('BOLT Simple Test', () => {
  it('should return expected #1', () => {
    const c = ['fastcity', 'bigbanana', 'xyz'];
    const x = [23, 23, 23];
    const y = [1, 10, 20];
    const q = ['fastcity', 'bigbanana', 'xyz'];
    expect(f(c, x, y, q)).toStrictEqual(['bigbanana', 'fastcity', 'bigbanana']);
  });
  it('should return expected #2', () => {
    const c = ['london', 'warsaw', 'hackerland'];
    const x = [1, 10, 20];
    const y = [1, 10, 10];
    const q = ['london', 'warsaw', 'hackerland'];
    expect(f(c, x, y, q)).toStrictEqual(['NONE', 'hackerland', 'warsaw']);
  });
  it('should return expected #3', () => {
    const c = ['green', 'red', 'blue', 'yellow', 'pink'];
    const x = [100, 200, 300, 400, 500];
    const y = [100, 200, 300, 400, 500];
    const q = ['green', 'red', 'blue', 'yellow', 'pink'];
    expect(f(c, x, y, q)).toStrictEqual(['NONE', 'NONE', 'NONE', 'NONE', 'NONE']);
  });
});

describe('My simple test', () => {
  it('should work on n = 1, m = 1, x[i]=y[i]=uniq(). Should return m * NONE.', () => {
    const n = 1;
    const m = 1;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => index);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-' + index);
    const res = Array.from(Array(m)).fill('NONE');
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 5, m = 1, x[i]=!uniq(), y[i]=uniq(). Should return q[...].', () => {
    const n = 5;
    const m = 1;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => 1);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-' + index);
    const res = ['city-1'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 5, m = 1, x[i]=uniq(), y[i]=!uniq(). Should return q[...].', () => {
    const n = 5;
    const m = 1;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => index);
    const y = Array.from(Array(n), (el, index) => 1);
    const q = Array.from(Array(m), (el, index) => 'city-' + index);
    const res = ['city-1'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 5, m = 2 m[1]=m[2], x[i]=uniq(), y[i]=uniq(). Should return q[...].', () => {
    const n = 5;
    const m = 2;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => index);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-0');
    const res = Array.from(Array(m)).fill('NONE');
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 5, m = 2 m[1]=m[2], x[i]=!uniq(), y[i]=uniq(). Should return q[...].', () => {
    const n = 5;
    const m = 2;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => 1);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-0');
    const res = ['city-1', 'city-1'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 5, m = 4 m[1]=m[2] && m[3] = m[4], x[i]=uniq(), y[i]=uniq(). Should return q[...].', () => {
    const n = 5;
    const m = 4;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => 1);
    const y = Array.from(Array(n), (el, index) => index);
    const q = ['city-0', 'city-0', 'city-4', 'city-4'];
    const res = ['city-1', 'city-1', 'city-3', 'city-3'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 5, m = 2 m[1]=m[2], x[i]=uniq(), y[i]=!uniq(). Should return q[...].', () => {
    const n = 5;
    const m = 2;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => index);
    const y = Array.from(Array(n), (el, index) => 1);
    const q = Array.from(Array(m), (el, index) => 'city-0');
    const res = ['city-1', 'city-1'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 1, m = 5, x[i]=uniq(), y[i]=uniq(). Should return m * NONE.', () => {
    const n = 1;
    const m = 5;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => index);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-' + index);
    const res = Array.from(Array(m)).fill('NONE');
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 1, m = 5, m=uniq(), x[i]=!uniq(), y[i]=uniq(). Should return m * NONE.', () => {
    const n = 1;
    const m = 5;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => 1);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-' + index);
    const res = Array.from(Array(m)).fill('NONE');
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 1, m = 5, m[i..m]=!uniq, x[i]=!uniq(), y[i]=uniq(). Should return m * NONE.', () => {
    const n = 1;
    const m = 5;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => 1);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-0');
    const res = Array.from(Array(m)).fill('NONE');
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should work on n = 2, m = 5, m[i..m]=!uniq, x[i]=!uniq(), y[i]=uniq(). Should return q[...].', () => {
    const n = 2;
    const m = 5;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => 1);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-0');
    const res = ['city-1', 'city-1', 'city-1', 'city-1', 'city-1'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
});

describe('Test for alphabeth sorting when distance is equal', () => {
  it('should correct sort alphabeticaly', () => {
    const n = 4;
    const m = 1;
    const c = ['x', 'ab', 'aba', 'abb'];
    const x = [2, 1, 2, 3];
    const y = [2, 2, 3, 2];
    const q = ['x'];
    const res = ['ab'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should correct sort alphabeticaly', () => {
    const n = 4;
    const m = 1;
    const c = ['x', 'aba', 'ab', 'abb'];
    const x = [2, 1, 2, 3];
    const y = [2, 2, 3, 2];
    const q = ['x'];
    const res = ['ab'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
  it('should correct sort alphabeticaly', () => {
    const n = 4;
    const m = 1;
    const c = ['x', 'y', 'aba', 'ab', 'abb'];
    const x = [2, 1, 1, 2, 3];
    const y = [2, 3, 2, 3, 2];
    const q = ['x', 'y'];
    const res = ['ab', 'ab'];
    expect(f(c, x, y, q)).toStrictEqual(res);
  });
});

describe('Test for big numbers', () => {
  it('should work on n = 10^5, m = 1000, x[i]=y[i]=uniq(). Should return m * NONE.', () => {
    const n = Math.pow(10, 5);
    const m = 100;
    const c = Array.from(Array(n), (el, index) => 'city-' + index);
    const x = Array.from(Array(n), (el, index) => index);
    const y = Array.from(Array(n), (el, index) => index);
    const q = Array.from(Array(m), (el, index) => 'city-' + index);
    const res = Array.from(Array(m)).fill('NONE');
    expect(f(c, x, y, q)).toStrictEqual(res);
  });

  // it('should work on n = 10^5, m = 10^5, x[i]=y[i]=uniq(). Should return m * NONE.', () => {
  //   const n = Math.pow(10, 5);
  //   const m = Math.pow(10, 5);
  //   const c = Array.from(Array(n), (el, index) => 'city-' + index);
  //   const x = Array.from(Array(n), (el, index) => index);
  //   const y = Array.from(Array(n), (el, index) => index);
  //   const q = Array.from(Array(m), (el, index) => 'city-' + index);
  //   const res = Array.from(Array(m)).fill('NONE');
  //   expect(f(c, x, y, q)).toStrictEqual(res);
  // });

  // TODO: Проверить максимальные значение при вырожденном случае x[i]=y[i]=uniq()
  // и максимальном количестве сравнений по алфавиту.
  //    ? - как сделать максимальное значение сравнений по алфавиту? желательно =
  //    10^5

  // TODO: Проверть максимальные значения x,y (max 10^9).
});
